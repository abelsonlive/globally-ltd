#!/bin/sh

logGroup="/ecs/globally-ltd-prod-ecs-task-game-server"
logStreams=$(aws --profile gltd logs describe-log-streams --log-group-name=$logGroup | jq '.logStreams[].logStreamName' -r)
for stream in $logStreams; do
  echo "Printing logs from stream '$stream'..."
  aws --profile gltd logs get-log-events --log-stream-name=$stream --log-group-name=$logGroup | jq '.events[] | "\(.timestamp/1000|strflocaltime("%Y-%m-%dT%H:%M:%S")): \(.message)"' -r
done
