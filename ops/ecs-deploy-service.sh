#!/bin/sh

echo "forcing new deployment of ecs service"
aws --profile gltd ecs update-service \
  --cluster globally-ltd-production-cluster  --service gltd-production-ecs-service --force-new-deployment |
  jq '.service.deployments[]| "\(.id): \(.status)"' -r
echo "done!"
echo "check on the status of the ECS tasks here:"
echo "https://console.aws.amazon.com/ecs/home?region=us-east-1#/clusters/globally-ltd-production-cluster/services/gltd-production-ecs-service/tasks"