# [🌍 globally.ltd 🌍](https://globally.ltd)

Multiplayer Rave Server

## Local Development

To develop locally ( assuming a Mac OS X machine), first install system dependencies via [homebrew](https://homebrew.sh)

```
brew bundle
```

### Client application

To be able to build the [client application](client/), you'll need to first enter its folder, and install the dependencies

```
cd client
yarn install
```

Now you can build the client and start the `local` server it by running:

```
yarn start
```

It will spawn the `webpack-dev-server`, listening on [http://localhost:3000](http://localhost:3000).


### Server application

To start the [server application](server/), the steps are similar to the `client`, except we use `npm` instead of `yarn` (TODO: standardize?)

Install the server's dependencies:

```
cd server
yarn install
```

Now you build can start the `local`, hot-reloading server with:

```
yarn start
```

It will spawn a web socket server, listening on [ws://localhost:2657](ws://localhost:2657).

### Local Docker Development

To test out the [Docker setup for the server](server/Dockerfile) locally, first build and run the image:

```
cd server
docker build . --build-arg env=local-docker
```

Note the resulting image ID and pass it into the run command:

```
docker run -p 2657:2657 <image-id>
```

Now, open another terminal and start the `development` client:

```
cd client/
yarn start
```

## Deployment

### TLDR

We use AWS S3 to host the `client` and [Heroku](https://heroku.com) to host the `server`. 

- Configure `aws-cli` to have the appropriate credentials for the `gltd` profile.
- Deploy the client to [dev.globally.ltd](https://dev.globally.ltd)
  - `make deploy-dev-client`
- Deploy the client to [globally.ltd](https://globally.ltd)
  - `make deploy-prod-client`
- Deploy the shared game server:
  - `make deploy-server`

### Deploy the Client Application

The client application is deployed to `S3` and distributed via `Cloudfront`.

#### Development

To deploy to the development site ([dev.globally.ltd](https://dev.globally.ltd)), which assumes that a game server is running on [https://globally-ltd.herokuapp.com/](https://globally-ltd.herokuapp.com/), run:

```
cd client/
yarn deploy-dev
```

#### Production

To deploy to the production site, ([globally.ltd](https://globally.ltd)), which assumes that a game server is running on [https://globally-ltd.herokuapp.com/](https://globally-ltd.herokuapp.com/), run:

```
cd client/
yarn deploy-prod
```

### Deploy the Server Application

The server is hosted on Heroku, you can deploy it by running

```
git push heroku master
```

### Start / Stop the AWS Medalive stream

The livestream is managed by AWS Medialive. You can start/stop the channel by running the following commands:

```
make start-medialive-channel
make stop-medialive-channel
```

## Resources

### Development Stack

- [(dev) Client S3 Bucket](https://console.aws.amazon.com/s3/buckets/dev.globally.ltd?region=us-east-1)
- [(dev) Client Cloudfront Distribution](https://console.aws.amazon.com/cloudfront/home?#distribution-settings:EFHUUIOP3O9P7)
- [(dev) Heroku Application](https://dashboard.heroku.com/apps/globally-ltd)

### Production Stack

- [(prod) Client S3 Bucket](https://console.aws.amazon.com/s3/buckets/globally.ltd?region=us-east-1)
- [(prod) Client Cloudfront Distribution](https://console.aws.amazon.com/cloudfront/home?#distribution-settings:E2DE23OS9HKLGJ)
- [(prod) Heroku Application](https://dashboard.heroku.com/apps/globally-ltd)

### Etc.
- [(etc) AWS Medialive Channel](https://console.aws.amazon.com/medialive/home?region=us-east-1#!/channels/2561059)
- [(etc) Flyzoo Chat Dashboard](http://dashboard.flyzoo.co/)
- [(etc) Route 53 Hosted Zone for \*.globally.ltd](https://console.aws.amazon.com/route53/home?#resource-record-sets:ZO30PRDJ1FL3K)

### References

- Game servers:
  - [Colyseus (Multiplayer Game Server For Node)](https://colyseus.io/)
  - [Colyseus/Babylon JS Example](https://github.com/endel/colyseus-babylonjs-boilerplate)
- 3D
  - [Three.JS](https://threejs.org/)
* Etc
  - [🌍](https://emojipedia.org/globe-showing-europe-africa/)
