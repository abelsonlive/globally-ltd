import React, { Suspense, useEffect } from 'react';
import { useThree } from 'react-three-fiber';
import * as THREE from 'three';
import TrackLight from '../../Common/Lights/TrackLight';
import "../../Common/UI/Player/Player.css";
import Stars from '../../Common/Utils/Stars';
import Rave from '../../Rave/Rave';
import "../Room.css";
import { RAVE_NAME } from './constants';
import Hall from './Hall';
import { MaterialsProvider } from './MaterialsContext';

export default function Scene({ content, hasEnteredWorld, ...props }) {
  const { scene } = useThree();

  useEffect(() => {
    scene.background = new THREE.Color( 0xb00ca2 );
  }, [])

  return <>
    <Rave name={RAVE_NAME} />
    <Stars count={1000} radius={2} colors={[0xffffff, 0xfffff0, 0xf9f1f1]} />
    <TrackLight intensity={.25} size={1} position={[0, 8, -5]} start={[0, 40, 100]} end={[0, 40, -100]} />
    <TrackLight intensity={.75} position={[0, 8, -5]} start={[0, 30, 100]} end={[0, 30, -100]} />
    <MaterialsProvider play={hasEnteredWorld}>
      <Suspense fallback={null}>
        <Hall />
      </Suspense>
    </MaterialsProvider>
  </>
}