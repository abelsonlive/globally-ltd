import React, { useContext, useMemo } from 'react';
import { useFrame, useLoader, useResource } from 'react-three-fiber';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import spaceIsThePlace from "../../Common/assets/objects/rooms/space-is-the-place.gltf";
import { MaterialsContext } from './MaterialsContext';

export default function Hall({ }) {
    const gltf = useLoader(GLTFLoader, spaceIsThePlace, loader => {
        const dracoLoader = new DRACOLoader()
        dracoLoader.setDecoderPath('/draco-gltf/')
        loader.setDRACOLoader(dracoLoader)
    })
    const [ref, room] = useResource()
    const { naiveGlass, linedCement, tron, liveStream, discoBall: discoBallMat, plant } = useContext(MaterialsContext);

    const [discoBallRef, discoBall] = useResource();

    const [wall, ceiling, floor, wallScreen1, wallScreen2, plants1, windowGlass1, windowPane1, skylightLargeGlass, skylightLargeFrame, skylightSmall, discoBallGeom, discoBallPos] = useMemo(() => {
        let w, c, f, ws1, ws2, p1, p2, wp1, wg1, slg, slf, ss, d, dp;
        gltf.scene.traverse(child => {
            if (child.geometry) {
                child.geometry.name = child.name;
                if (child.name == "Hall_0") {
                    c = child.geometry.clone()
                }
                if (child.name == "Hall_1") {
                    w = child.geometry.clone();
                }
                if (child.name == "Hall_2") {
                    f = child.geometry.clone();
                }
                if (child.name == "Hall_3") {
                    ws1 = child.geometry.clone();
                }
                if (child.name == "Hall_4") {
                    ws2 = child.geometry.clone();
                }
                if (child.name == "Plane842_Plant_12_0") {
                    p1 = child.geometry.clone();
                }
                if (child.name == "Window1_0") {
                    wg1 = child.geometry.clone();
                }
                if (child.name == "Window1_1") {
                    wp1 = child.geometry.clone();
                }
                if (child.name == "SkylightLarge_0") {
                    slg = child.geometry.clone();
                }
                if (child.name == "SkylightLarge_1") {
                    slf = child.geometry.clone();
                }
                if (child.name == "SkylightSmall") {
                    ss = child.geometry.clone();
                }
                if (child.name == "DiscoBall") {
                    dp = child.position;
                    d = child.geometry.clone();

                }
            }
        })
        if (!w || !f || !c || !ws1 || !ws2 || !p1 || !wg1 || !wp1 || !slg || !slf || !ss || !d) {
            console.error("Didn't find geometries.")
        }
        return [w, c, f, ws1, ws2, p1, wg1, wp1, slg, slf, ss, d, dp]
    });

    useFrame(() => {
        discoBall.rotation.y += .001;
    })

    return <group ref={ref}>
        <mesh material={linedCement}>
            <bufferGeometry attach="geometry" {...wall} />
        </mesh>
        <mesh material={linedCement}>
            <bufferGeometry attach="geometry" {...floor} />
        </mesh>
        <mesh material={linedCement}>
            <bufferGeometry attach="geometry" {...ceiling} />
        </mesh>
        <mesh material={tron}>
            <bufferGeometry attach="geometry" {...wallScreen1} />
        </mesh>
        <mesh material={liveStream}>
            <bufferGeometry attach="geometry" {...wallScreen2} />
        </mesh>
        <mesh material={plant}>
            <bufferGeometry attach="geometry" {...plants1} />
        </mesh>
        <mesh material={linedCement}>
            <bufferGeometry attach="geometry" {...windowPane1} />
        </mesh>
        <mesh material={naiveGlass}>
            <bufferGeometry attach="geometry" {...windowGlass1} />
        </mesh>
        <mesh material={naiveGlass}>
            <bufferGeometry attach="geometry" {...skylightLargeGlass} />
        </mesh>
        <mesh material={linedCement}>
            <bufferGeometry attach="geometry" {...skylightLargeFrame} />
        </mesh>
        <mesh material={linedCement}>
            <bufferGeometry attach="geometry" {...skylightSmall} />
        </mesh>
        <mesh material={discoBallMat} ref={discoBallRef} position={discoBallPos}>
            <bufferGeometry attach="geometry" {...discoBallGeom} />
        </mesh>
    </group>

}
