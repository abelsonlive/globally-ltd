import React, { useEffect, useState } from 'react';
import * as THREE from 'three';
import { useResource } from 'react-three-fiber';
import LinedCement from '../../Common/Materials/LinedCement';
import NaiveGlass from '../../Common/Materials/NaiveGlass';
import { TronMaterial } from '../../Common/Materials/TronMaterial';
import LiveStream from '../../Common/Materials/LiveStream';
import { LIVESTREAM_URL } from './constants';
import DiscoBall01 from '../../Common/Materials/DiscoBall01';
import Plant1 from '../../Common/Materials/Plant1';
const MaterialsContext = React.createContext([{}, () => { }]);

const MaterialsProvider = ({ play, ...props }) => {
    const [loaded, setLoaded] = useState(false);

    const [linedCementRef, linedCement] = useResource();
    const [naiveGlassRef, naiveGlass] = useResource();
    const [tronRef, tron] = useResource();
    const [liveStreamRef, liveStream] = useResource();
    const [discoBallRef, discoBall] = useResource();
    const [plantRef, plant] = useResource();

    const materials = {
        linedCement,
        naiveGlass,
        tron,
        liveStream,
        discoBall,
        plant,
    }

    useEffect(() => {
        const allMats = Object.values(materials);
        const loadedMats = allMats.filter(mat => mat);
        setLoaded(allMats.length == loadedMats.length);
    })

    return <MaterialsContext.Provider value={{ loaded, ...materials }}>
        <LinedCement materialRef={linedCementRef} color={"black"} side={THREE.DoubleSide} textureRepeat={{ x: 16, y: 16 }} />
        <NaiveGlass materialRef={naiveGlassRef} side={THREE.DoubleSide} opacity={.1} shininess={75} color={"blue"} />
        <TronMaterial materialRef={tronRef} />
        <LiveStream materialRef={liveStreamRef} side={THREE.DoubleSide} src={LIVESTREAM_URL} play={play} />
        <DiscoBall01 materialRef={discoBallRef} />
        <Plant1 materialRef={plantRef} />
        {props.children}
    </MaterialsContext.Provider>
}

export { MaterialsContext, MaterialsProvider };



