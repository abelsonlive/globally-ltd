import { assetPath } from "../../Common/Utils/assets";

export const assetPathSpaceIsThePlace = (p) => {
    return assetPath("space-is-the-place/" + p);
}