import { assetPathSpaceIsThePlace } from './utils';

export const HALL_URL = assetPathSpaceIsThePlace("objects/room/room.gltf")
export const LIVESTREAM_URL = "https://stream.globally.ltd/live/dev.globally.ltd.m3u8";
export const RAVE_NAME = "game";
