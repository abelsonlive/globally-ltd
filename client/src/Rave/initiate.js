import * as THREE from 'three';


export function initiateAvatar({ }) {
    const geometry = new THREE.SphereGeometry(2, 4, 4);
    const material = new THREE.MeshBasicMaterial({
        color: '#' + Math.floor(Math.random() * 16777215).toString(16)
    });
    const avatar = new THREE.Mesh(geometry, material, {receiveShadow: true});
    return avatar;
}

export function initiateRaver({
    rave,
    everyone,
    scene,
    camera,
    setKey,
    ...props
}) {
    const cameraOffset = props.cameraOffset ? props.cameraOffset : { y: 4, z: 12 };
    const speed = props.speed ? props.speed : 2;

    rave.state.players.onAdd = function (player, key) {
        // TODO -- haven't figured out how to make the avatar into a React component - it copies to all client sessions the same object when I've tried.
        const avatar = initiateAvatar({})
        scene.add(avatar)
        everyone[key] = avatar;
        everyone[key].position.set(player.position.x, player.position.y, player.position.z);
        if (key === rave.sessionId) {
            setKey(key)
            camera.position.set(player.position.x, player.position.y + cameraOffset.y, player.position.z + cameraOffset.z);
            camera.lookAt(everyone[key].position);
        }
    };

    rave.state.players.onChange = function (player, key) {
        everyone[key].position.set(player.position.x, player.position.y, player.position.z);
        if (key === rave.sessionId) {
            camera.position.set(player.position.x, player.position.y + cameraOffset.y, player.position.z + cameraOffset.z);
            camera.lookAt(everyone[key].position);
        }
    };

    rave.state.players.onRemove = function (player, key) {
        scene.remove(everyone[key]);
        delete everyone[key];
    };

    rave.onStateChange(state => {
    });

    // Keyboard listeners
    const keyboard = { x: 0, y: 0 };
    window.addEventListener("keydown", function (e) {
        if (e.which === 37) {
            keyboard.x = -1 * speed;
        } else if (e.which === 39) {
            keyboard.x = speed;
        } else if (e.which === 40) {
            keyboard.y = -1 * speed;
        } else if (e.which === 38) {
            keyboard.y = speed;
        }
        rave.send(["key", keyboard]);
    });

    window.addEventListener("keyup", function (e) {
        if (e.which === 37) { // left
            keyboard.x = 0;
        } else if (e.which === 39) { // right
            keyboard.x = 0;
        } else if (e.which === 40) { // down (TODO -- this is inverted)
            keyboard.y = 0;
        } else if (e.which === 38) { // up  (TODO -- this is inverted)
            keyboard.y = 0;
        }
        rave.send(["key", keyboard]);
    });

}