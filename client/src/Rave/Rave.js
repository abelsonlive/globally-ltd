import React, { useEffect, useMemo, useRef, useState, useContext } from "react";
import Raver from "./Raver";
import { RaveProvider } from "./RaveContext";
import { namespacedRoom } from "./utils";

export default function Rave({ name, ...props }) {
  return (
    <RaveProvider name={namespacedRoom(name)}>
      {useMemo(() => {
        return <Raver {...props} />;
      })}
    </RaveProvider>
  );
};
