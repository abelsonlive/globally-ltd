import { useEffect, useContext, useMemo, useState } from 'react';
import { useThree } from 'react-three-fiber';
import * as THREE from 'three';
import { initiateRaver } from './initiate';
import { RaveContext } from './RaveContext';

export function useRaver({ ...props }) {
  const { rave } = useContext(RaveContext);
  const { scene, camera } = useThree();
  const [key, setKey] = useState()

  const everyone = useMemo(() => {
    // TODO: this is likely the location of some object 
    // that has more features and can encapsulate more of 
    // the properties that get accessed when
    // setting up the 'room' in useRaver
    return {};
  }, [])

  useEffect(() => {
    if (!rave) return;
    initiateRaver({ rave, everyone, scene, camera, setKey, ...props })
  });

  return { position: key in everyone ? everyone[key].position : undefined };
}