import path from "path";
import http from "http";
import express from "express";
import cors from "cors";
import { Server } from "colyseus";

import { GameRoom } from "./rooms/GameRoom";

export const port = process.env.PORT || 2657;

const app = express();

app.use(cors());
app.use(express.json());
// NOTE: uncomment this to server the client from the server
//       We're not currently doing this because heroku has issues 
//       with webpack.
// app.use(express.static(path.join(__dirname, "..", "..", "client", "dist")));

// Create HTTP & WebSocket servers
const server = http.createServer(app);
const gameServer = new Server({
    server: server,
    express: app
});
const ROOMS = [
  {name: "game", handler: GameRoom}
]
// namespace rooms for dev
ROOMS.forEach((room) => {
  gameServer.define(room.name, room.handler);
  gameServer.define(room.name + "-dev", room.handler);
})

server.listen(port);
console.log(`Listening on ${ port }`)